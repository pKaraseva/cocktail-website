package ru.nsu.fit.alcoweb.alcobackend;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import ru.nsu.fit.alcoweb.alcobackend.model.api.Ingredient;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RecipeContent;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RecipeShortDescription;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RecipeSearchParameters;
import ru.nsu.fit.alcoweb.alcobackend.recipe.RecipeService;

import java.util.Objects;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@PropertySource("classpath:application-test.properties")
class AlcoBackendApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String getUrl(String path) {
        return "http://localhost:" + port + "/" + path;
    }

    @Autowired
    RecipeService recipeService;

    @Test
    void contextLoads() {
        System.out.println(1);
    }

    @Test
    @Disabled
    void recipeCreated() {
        var content = new RecipeContent()
                .addStepsItem("Make")
                .addStepsItem("Drink")
                .addStepsItem("Done!")
                .addIngredientsItem(new Ingredient()
                        .label("Tequila")
                        .value("A lot of tequila"))
                .addIngredientsItem(new Ingredient()
                        .label("Rum")
                        .value("A bit of rum"))
                .addTagsItem("easy")
                .addTagsItem("tasty")
                .iconUrl("http://url.image")
                .title("Rum + Tequila!");
        ResponseEntity<RecipeShortDescription> description = restTemplate.postForEntity(getUrl("recipe/"),
                content, RecipeShortDescription.class);
        // long id = recipeService.createRecipe(DTOConverter.fromDto(content));
        Assertions.assertEquals("Rum + Tequila!",
                recipeService.getRecipe(Objects.requireNonNull(description.getBody()).getId()).getTitle());
    }

    @Test
    @Disabled
    void recipeSearch() {
        var content = new RecipeContent()
                .addStepsItem("Make")
                .addStepsItem("Drink")
                .addStepsItem("Done!")
                .addIngredientsItem(new Ingredient()
                        .label("Tequila")
                        .value("A lot of tequila"))
                .addIngredientsItem(new Ingredient()
                        .label("Rum")
                        .value("A bit of rum"))
                .addTagsItem("easy")
                .addTagsItem("tasty")
                .iconUrl("http://url.image")
                .title("Rum + Tequila!");
        long id = recipeService.createRecipe(DTOConverter.fromDto(content, "TEST_USERNAME"));
        Assertions.assertEquals("Rum + Tequila!", recipeService.findRecipes(new RecipeSearchParameters()).get(1).getTitle());
    }

}

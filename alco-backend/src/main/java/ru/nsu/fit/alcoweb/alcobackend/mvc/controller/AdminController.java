package ru.nsu.fit.alcoweb.alcobackend.mvc.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.alcoweb.alcobackend.api.AdminApi;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RequestStatus;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.User;
import ru.nsu.fit.alcoweb.alcobackend.user.UserRole;
import ru.nsu.fit.alcoweb.alcobackend.user.UserService;

import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.username;

@RestController
public class AdminController implements AdminApi {

    private final UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @CrossOrigin
    @Override
    public ResponseEntity<List<String>> adminModeratorPendingGet() {
        userService.assertHasRole(username(), UserRole.ADMIN);
        return ResponseEntity.ok(userService.findByModeratorStatus(RequestStatus.PENDING).stream()
                .map(User::getUsername)
                .collect(Collectors.toList()));
    }

    @CrossOrigin
    @Override
    public ResponseEntity<Void> deleteModeratorRights(String username) {
        userService.assertHasRole(username(), UserRole.ADMIN);
        userService.removeModeratorRole(username);
        return ResponseEntity.ok(null);
    }

    @CrossOrigin
    @Override
    public ResponseEntity<List<String>> getModeratorsList() {
        userService.assertHasRole(username(), UserRole.MODERATOR);
        return ResponseEntity.ok(userService.findByModeratorStatus(RequestStatus.ACCEPTED).stream()
                .map(User::getUsername)
                .collect(Collectors.toList()));
    }

    @CrossOrigin
    @Override
    public ResponseEntity<Void> makeUserModerator(String username) {
        userService.assertHasRole(username(), UserRole.ADMIN);
        userService.grantModeratorRole(username);
        return ResponseEntity.ok(null);
    }
}

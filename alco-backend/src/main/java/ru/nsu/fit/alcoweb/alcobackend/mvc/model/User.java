package ru.nsu.fit.alcoweb.alcobackend.mvc.model;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import ru.nsu.fit.alcoweb.alcobackend.user.UserRole;

import static org.hibernate.annotations.LazyCollectionOption.TRUE;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    private String password;

    @ElementCollection(targetClass = UserRole.class, fetch = FetchType.EAGER)
    @JoinTable(name = "userRoles", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "roles", nullable = false)
    @Enumerated(EnumType.STRING)
    private List<UserRole> userRoles;

    private RequestStatus moderationRequest;

    @CollectionTable
    @ManyToMany
    @LazyCollection(TRUE)
    public List<Recipe> favourites;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public List<Recipe> getFavourites() {
        return favourites;
    }

    public void setFavourites(List<Recipe> favourites) {
        this.favourites = favourites;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public RequestStatus getModerationRequest() {
        return moderationRequest;
    }

    public void setModerationRequest(RequestStatus moderationRequest) {
        this.moderationRequest = moderationRequest;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", userRoles=" + userRoles +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        public List<Recipe> favourites;
        private Long id;
        private String username;
        private String password;
        private List<UserRole> userRoles;
        private RequestStatus moderationRequest;

        private Builder() {
        }

        public static Builder anUser() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withUserRoles(List<UserRole> userRoles) {
            this.userRoles = userRoles;
            return this;
        }

        public Builder withModerationRequest(RequestStatus moderationRequest) {
            this.moderationRequest = moderationRequest;
            return this;
        }

        public Builder withFavourites(List<Recipe> favourites) {
            this.favourites = favourites;
            return this;
        }

        public User build() {
            User user = new User();
            user.setId(id);
            user.setUsername(username);
            user.setPassword(password);
            user.setUserRoles(userRoles);
            user.setModerationRequest(moderationRequest);
            user.setFavourites(favourites);
            return user;
        }
    }
}

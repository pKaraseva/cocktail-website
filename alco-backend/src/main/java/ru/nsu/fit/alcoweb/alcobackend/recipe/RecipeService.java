package ru.nsu.fit.alcoweb.alcobackend.recipe;

import org.apache.commons.lang3.StringUtils;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.Recipe;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RecipeIngredient;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RecipeSearchParameters;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RequestStatus;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RecipeService {

    private final RecipeRepository recipeRepository;

    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public Recipe getRecipe(long id) {
        return recipeRepository.findById(id).orElse(null);
    }

    public void deleteRecipe(long id) {
        recipeRepository.deleteById(id);
    }

    public long createRecipe(Recipe recipe) {
        recipe.setPublicationStatus(RequestStatus.NONE);
        recipe.setUpdated(Date.from(Instant.now()));
        return recipeRepository.save(recipe).getId();
    }

    public void updateRecipe(Recipe recipe, long id) {
        recipe.setUpdated(Date.from(Instant.now()));
        recipe.setPublicationStatus(RequestStatus.NONE);
        recipe.setId(id);
        recipeRepository.save(recipe);
    }


    public boolean sendToModerationRecipe(long id) {
        var recipe = recipeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No recipe found with id " + id));
        recipe.setPublicationStatus(RequestStatus.PENDING);
        recipeRepository.save(recipe);
        return true;
    }

    public void approveRecipe(long id) {
        var recipe = recipeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No recipe found with id " + id));
        recipe.setPublicationStatus(RequestStatus.ACCEPTED);
        recipeRepository.save(recipe);
    }

    public void declineRecipe(long id) {
        var recipe = recipeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No recipe found with id " + id));
        recipe.setPublicationStatus(RequestStatus.DECLINED);
        recipeRepository.save(recipe);
    }

    public List<Recipe> findByStatus(RequestStatus status) {
        return recipeRepository.findAllByPublicationStatus(status);
    }

    public List<Recipe> findByUserAndStatus(String username, RequestStatus status) {
        return recipeRepository.findAllByAuthorAndPublicationStatus(username, status);
    }

    public List<Recipe> findRecipes(RecipeSearchParameters searchParameters) {
        String query = Objects.requireNonNullElse(searchParameters.getQuery(), "");
        List<Recipe> recipes;
        if (StringUtils.isNotEmpty(searchParameters.getAuthor())) {
            recipes = recipeRepository.findAllByTitleContainingAndAuthorAndPublicationStatus(
                    query,
                    searchParameters.getAuthor(),
                    RequestStatus.ACCEPTED);
        } else {
            recipes = recipeRepository.findAllByTitleContainingAndPublicationStatus(query,
                    RequestStatus.ACCEPTED);
        }
        int limit = Objects.requireNonNullElse(searchParameters.getLimit(), 12L).intValue();
        int offset = Objects.requireNonNullElse(searchParameters.getOffset(), 0L).intValue();
        return applyPoorManPaging(recipes.stream()
                        .filter(r -> StringUtils.isEmpty(searchParameters.getTag()) ||
                                r.getTags().contains(searchParameters.getTag()))
                        .filter(r -> StringUtils.isEmpty(searchParameters.getIngredient()) ||
                                r.getIngredients().stream()
                                        .map(RecipeIngredient::getLabel)
                                        .anyMatch(i -> i.equals(searchParameters.getIngredient()))),
                limit, offset);
    }

    private List<Recipe> applyPoorManPaging(Stream<Recipe> recipes, int limit, int offset) {
        return recipes.skip(offset).limit(limit).collect(Collectors.toList());
    }

    public void rateRecipe(Long recipeId, Integer rating, String username) {
        var recipe = recipeRepository.findById(recipeId)
                .orElseThrow(() -> new EntityNotFoundException("No recipe found with id " + recipeId));
        recipe.getRates().put(username, rating);
        recipeRepository.save(recipe);
    }
}

package ru.nsu.fit.alcoweb.alcobackend.mvc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.alcoweb.alcobackend.DTOConverter;
import ru.nsu.fit.alcoweb.alcobackend.api.RecipeApi;
import ru.nsu.fit.alcoweb.alcobackend.model.api.DetailedRecipeDescription;
import ru.nsu.fit.alcoweb.alcobackend.model.api.ModelApiResponse;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RecipeContent;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RecipeShortDescription;
import ru.nsu.fit.alcoweb.alcobackend.model.api.SearchRequest;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RequestStatus;
import ru.nsu.fit.alcoweb.alcobackend.recipe.RecipeService;
import ru.nsu.fit.alcoweb.alcobackend.user.UserRole;
import ru.nsu.fit.alcoweb.alcobackend.user.UserService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.ok;
import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.username;

@RestController
public class RecipeController implements RecipeApi {

    Logger log = LoggerFactory.getLogger(RecipeController.class);

    private final RecipeService recipeService;
    private final UserService userService;


    @Autowired
    public RecipeController(RecipeService recipeService, UserService userService) {
        this.recipeService = recipeService;
        this.userService = userService;
    }

    @CrossOrigin
    @Override
    public ResponseEntity<DetailedRecipeDescription> viewRecipe(Long recipeId) {
        var user = userService.loadUser(username());
        var recipe =
                Optional.ofNullable(recipeService.getRecipe(recipeId))
                        .map(r -> DTOConverter.toDetailedRecipeDto(r, user));
        return ResponseEntity.of(recipe);
    }

    @CrossOrigin
    @Override
    public ResponseEntity<RecipeShortDescription> createRecipe(RecipeContent body) {
        assertHasRole(UserRole.USER);
        var user = userService.loadUser(username());
        var id = recipeService.createRecipe(DTOConverter.fromDto(body, username()));
        if (id > 0) {
            return ResponseEntity.ok(DTOConverter.toShortDescription(recipeService.getRecipe(id), user));
        } else {
            throw new IllegalStateException("Failed to create a recipe");
        }
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> deleteRecipe(Long recipeId) {
        assertHasRole(UserRole.USER);
        if (userService.hasRole(username(), UserRole.MODERATOR) ||
                Objects.equals(recipeService.getRecipe(recipeId).getAuthor(), username())) {
            recipeService.deleteRecipe(recipeId);
            return ResponseEntity.ok(ok());
        } else {
            throw new AccessDeniedException("Access denied");
        }
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> editRecipe(Long recipeId, RecipeContent body) {
        assertHasRole(UserRole.USER);
        if (userService.hasRole(username(), UserRole.MODERATOR) ||
                Objects.equals(recipeService.getRecipe(recipeId).getAuthor(), username())) {
            var recipe = recipeService.getRecipe(recipeId);
            if (recipe == null) {
                throw new EntityNotFoundException();
            }
            recipe.setIngredients(body.getIngredients().stream()
                    .map(DTOConverter::fromDto).collect(Collectors.toList()));
            recipe.setSteps(body.getSteps());
            recipe.setTags(body.getTags());
            recipe.setTitle(body.getTitle());
            recipe.setIconUrl(body.getIconUrl());
            recipeService.updateRecipe(recipe, recipeId);
            return ResponseEntity.ok(ok());
        }
        throw new AccessDeniedException("Access denied");
    }

    @CrossOrigin
    @Override
    public ResponseEntity<List<RecipeShortDescription>> findRecipes(SearchRequest body) {
        if (body == null) {
            body = new SearchRequest();
        }
        var user = userService.loadUser(username());
        return ResponseEntity.ok(recipeService.findRecipes(DTOConverter.fromDto(body))
                .stream()
                .map(r -> DTOConverter.toShortDescription(r, user))
                .collect(Collectors.toList()));
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> declineRecipe(Long recipeId) {
        assertHasRole(UserRole.MODERATOR);
        recipeService.declineRecipe(recipeId);
        return ResponseEntity.ok(ok());
    }

    @CrossOrigin
    @Override
    public ResponseEntity<List<RecipeShortDescription>> getOwnRecipes(String status) {
        assertHasRole(UserRole.USER);
        var user = userService.loadUser(username());
        var statusEnum = RequestStatus.valueOf(status.toUpperCase(Locale.ROOT));
        return ResponseEntity.ok(recipeService.findByUserAndStatus(username(), statusEnum)
                .stream()
                .map(r -> DTOConverter.toShortDescription(r, user))
                .collect(Collectors.toList()));
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> publishRecipe(Long recipeId) {
        if (hasRole(UserRole.MODERATOR)) {
            assertHasRole(UserRole.MODERATOR);
            recipeService.approveRecipe(recipeId);
        } else {
            assertHasRole(UserRole.USER);
            var recipe = recipeService.getRecipe(recipeId);
            if (recipe == null) {
                throw new EntityNotFoundException();
            }
            if (!username().equals(recipe.getAuthor())) {
                throw new AccessDeniedException("Access denied");
            }
            recipeService.sendToModerationRecipe(recipeId);
        }
        return ResponseEntity.ok(ok());
    }

    @CrossOrigin
    @Override
    public ResponseEntity<List<RecipeShortDescription>> unpublishedRecipes() {
        assertHasRole(UserRole.MODERATOR);
        var user = userService.loadUser(username());
        return ResponseEntity.ok(recipeService.findByStatus(RequestStatus.PENDING).stream()
                .map(r -> DTOConverter.toShortDescription(r, user))
                .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<ModelApiResponse> rateRecipe(Long recipeId, Integer rating) {
        assertHasRole(UserRole.USER);
        if (rating > 5 || rating < 0){
            throw new IllegalArgumentException("Rating expected to be between 0 and 5");
        }
        recipeService.rateRecipe(recipeId, rating, username());
        return ResponseEntity.ok(ok());
    }

    private void assertHasRole(UserRole role) {
        log.info("Username extracted from session is: '" + username() + "'");
        userService.assertHasRole(username(), role);
    }

    private boolean hasRole(UserRole role) {
        log.info("Username extracted from session is: '" + username() + "'");
        return userService.hasRole(username(), role);
    }
}

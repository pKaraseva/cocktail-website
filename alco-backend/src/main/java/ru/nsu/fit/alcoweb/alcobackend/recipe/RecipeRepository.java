package ru.nsu.fit.alcoweb.alcobackend.recipe;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.Recipe;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RequestStatus;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
    List<Recipe> findAllByAuthorAndPublicationStatus(String author, RequestStatus publicationStatus);

    List<Recipe> findAllByPublicationStatus(RequestStatus publicationStatus);

    List<Recipe> findAllByTitleContainingAndPublicationStatus(String title, RequestStatus publicationStatus);

    List<Recipe> findAllByTitleContainingAndAuthorAndPublicationStatus(String title, String author, RequestStatus publicationStatus);
}

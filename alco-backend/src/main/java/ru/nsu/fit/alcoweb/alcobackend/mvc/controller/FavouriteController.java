package ru.nsu.fit.alcoweb.alcobackend.mvc.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.alcoweb.alcobackend.DTOConverter;
import ru.nsu.fit.alcoweb.alcobackend.api.FavouriteApi;
import ru.nsu.fit.alcoweb.alcobackend.model.api.ModelApiResponse;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RecipeShortDescription;
import ru.nsu.fit.alcoweb.alcobackend.user.UserRole;
import ru.nsu.fit.alcoweb.alcobackend.user.UserService;

import java.util.List;
import java.util.stream.Collectors;

import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.ok;
import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.username;

@RestController
public class FavouriteController implements FavouriteApi {
    private final UserService userService;

    public FavouriteController(UserService userService) {
        this.userService = userService;
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> addFavouriteRecipe(Long recipeId) {
        assertLoggedIn();
        userService.addFavourite(username(), recipeId);
        return ResponseEntity.ok(ok());
    }

    @CrossOrigin
    @Override
    public ResponseEntity<List<RecipeShortDescription>> getFavouriteRecipes() {
        assertLoggedIn();
        var user = userService.loadUser(username());
        return ResponseEntity.ok(userService.getFavourites(username()).stream()
                .map(f -> DTOConverter.toShortDescription(f, user))
                .collect(Collectors.toList()));
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> removeFavouriteRecipe(Long recipeId) {
        assertLoggedIn();
        userService.removeFavourite(username(), recipeId);
        return ResponseEntity.ok(ok());
    }

    private void assertLoggedIn() {
        userService.assertHasRole(username(), UserRole.USER);
    }
}

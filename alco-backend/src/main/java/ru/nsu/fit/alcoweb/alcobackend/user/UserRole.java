package ru.nsu.fit.alcoweb.alcobackend.user;

import javax.persistence.Id;

public enum UserRole {
    USER(1L),
    MODERATOR(2L),
    ADMIN(3L);

    UserRole(Long id) {
        this.id = id;
    }

    private Long id;


    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}

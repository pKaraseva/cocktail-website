package ru.nsu.fit.alcoweb.alcobackend.mvc.model;

public class RecipeSearchParameters {
    public enum Order {
        DATE, RATING
    }

    private Long limit;
    private Long offset;
    private String author;
    private String query;
    private String tag;
    private String ingredient;

    public RecipeSearchParameters() {
    }

    public RecipeSearchParameters(Long limit, Long offset, String author, String query, String tag, String ingredient) {
        this.limit = limit;
        this.offset = offset;
        this.author = author;
        this.query = query;
        this.tag = tag;
        this.ingredient = ingredient;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public String toString() {
        return "RecipeSearchParameters{" +
                "limit=" + limit +
                ", offset=" + offset +
                ", author='" + author + '\'' +
                ", query='" + query + '\'' +
                '}';
    }
}

package ru.nsu.fit.alcoweb.alcobackend.user;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.Recipe;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RequestStatus;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.User;
import ru.nsu.fit.alcoweb.alcobackend.recipe.RecipeService;

@Service
public class UserService implements UserDetailsService {
    Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final RecipeService recipeService;

    @Autowired
    public UserService(UserRepository userRepository, RecipeService recipeService) {

        this.userRepository = userRepository;
        this.recipeService = recipeService;
    }

    @PostConstruct
    private void createDebugUsers() {
        if (loadUser("test") == null) {
            log.info(createUser("test", "test123").toString());
        }
        if (loadUser("admin") == null) {
            log.info(createUser("admin", "admin").toString());
            grantAdminRole("admin");
        }
    }

    @Override
    public UserPrincipal loadUserByUsername(String username) {
        Writer buffer = new StringWriter();
        PrintWriter pw = new PrintWriter(buffer);
        new Exception().printStackTrace(pw);
        log.info(buffer.toString());
        User user = userRepository.findByUsername(username);
        if (user == null) {
            log.info("User {} not found", username);
            throw new UsernameNotFoundException(username);
        }
        log.info(user.toString());
        return new UserPrincipal(user);
    }

    public User loadUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional
    public User createUser(String username, String password) {
        if (userRepository.findByUsername(username) != null) {
            throw new RuntimeException("User already exists");
        }
        return userRepository.save(User.builder()
                .withUsername(username)
                .withPassword(password)
                .withUserRoles(List.of(UserRole.USER))
                .withModerationRequest(RequestStatus.NONE)
                .build());
    }

    @Transactional
    public void addRole(String username, UserRole role) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User doesn't exists");
        }
        List<UserRole> roles = user.getUserRoles();
        if (roles.contains(role)) {
            return;
        }
        roles = new ArrayList<>(roles);
        roles.add(role);
        user.setUserRoles(roles);
        userRepository.save(user);
    }

    @Transactional
    public void removeRole(String username, UserRole role) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User doesn't exists");
        }
        List<UserRole> roles = user.getUserRoles();
        roles = new ArrayList<>(roles);
        roles.remove(role);
        user.setUserRoles(roles);
        userRepository.save(user);
    }

    @Transactional
    public void addFavourite(String username, long id) {
        var recipe = recipeService.getRecipe(id);
        if (recipe == null) {
            throw new EntityNotFoundException("Recipe not found");
        }
        var user = userRepository.findByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("User not found");
        }
        var favourites = new ArrayList<>(user.getFavourites());
        if (favourites.contains(recipe)) return;
        favourites.add(recipe);
        user.setFavourites(favourites);
        userRepository.save(user);
    }

    @Transactional
    public void removeFavourite(String username, long id) {
        var user = userRepository.findByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("User not found");
        }
        var favourites = new ArrayList<>(user.getFavourites());
        favourites.removeIf(recipe -> recipe.getId() == id);
        user.setFavourites(favourites);
        userRepository.save(user);
    }

    @Transactional
    public List<Recipe> getFavourites(String username) {
        var user = userRepository.findByUsername(username);
        return Optional.ofNullable(user.getFavourites()).orElse(List.of());
    }

    public void assertHasRole(String username, UserRole role) {
        var user = loadUserByUsername(username);
        if (username.equals("anon") || user == null || user.getAuthorities() == null || user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .noneMatch(r -> role.name().equals(r))) {
            throw new AccessDeniedException("Access denied");
        }
    }

    public boolean hasRole(String username, UserRole role) {
        if (username.equals("anon")) return false;
        var user = loadUserByUsername(username);
        return user != null && user.getAuthorities() != null && user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .anyMatch(r -> role.name().equals(r));
    }

    public List<User> findByModeratorStatus(RequestStatus status) {
        return userRepository.findAllByModerationRequest(status);
    }

    public void requestModeratorRole(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User doesn't exists");
        }
        user.setModerationRequest(RequestStatus.PENDING);
        userRepository.save(user);
    }

    public void grantModeratorRole(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User doesn't exists");
        }
        user.setModerationRequest(RequestStatus.ACCEPTED);
        userRepository.save(user);
        addRole(username, UserRole.MODERATOR);
    }


    public void removeModeratorRole(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User doesn't exists");
        }
        user.setModerationRequest(RequestStatus.DECLINED);
        userRepository.save(user);
        removeRole(username, UserRole.MODERATOR);
    }

    public void grantAdminRole(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User doesn't exists");
        }
        user.setModerationRequest(RequestStatus.ACCEPTED);
        userRepository.save(user);
        addRole(username, UserRole.MODERATOR);
        addRole(username, UserRole.ADMIN);
    }


    public void removeAdminRole(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User doesn't exists");
        }
        userRepository.save(user);
        removeRole(username, UserRole.MODERATOR);
        addRole(username, UserRole.ADMIN);

    }
}

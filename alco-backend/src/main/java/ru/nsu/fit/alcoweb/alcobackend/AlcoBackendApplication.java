package ru.nsu.fit.alcoweb.alcobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlcoBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlcoBackendApplication.class, args);
    }

}


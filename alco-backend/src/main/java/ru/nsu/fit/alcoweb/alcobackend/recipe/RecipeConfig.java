package ru.nsu.fit.alcoweb.alcobackend.recipe;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.Recipe;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RecipeIngredient;

@Configuration
public class RecipeConfig {

    private final static Logger log = LoggerFactory.getLogger(RecipeConfig.class);


    @Bean
    List<Recipe> recipes() {
        return List.of(
                Recipe.builder()
                        .withTitle("Mojito")
                        .withAuthor("Me")
                        .withIconUrl("https://pics.jpg")
                        .withSteps(List.of("1", "2", "3"))
                        .withUpdated(new Date())
                        .withTags("easy", "tasty")
                        .withDescription("Even my grandma liked it!")
                        .withId(1L)
                        .withIngredients(List.of(
                                new RecipeIngredient("Mint", "Just mint"),
                                new RecipeIngredient("Rum", "Just Rum")))
                        .build());
    }

    @Bean
    @Transactional
    public RecipeService RecipeService(RecipeRepository recipeRepository, List<Recipe> recipes) {
        var service = new RecipeService(recipeRepository);
        recipes.forEach(r -> log.info(service.getRecipe(service.createRecipe(r)).toString()));
        return service;
    }
}

package ru.nsu.fit.alcoweb.alcobackend.mvc.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "recipes")
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "label")
    private String title;

    @Column(name = "description")
    private String description;

    @ElementCollection
    @Column(name = "tags")
    private List<String> tags = new ArrayList<>();

    @Column(name = "ingredients")
    @OneToMany(cascade = {CascadeType.ALL})
    private List<RecipeIngredient> ingredients = new ArrayList<>();

    @Column(name = "updated")
    private Date updated;

    @Column(name = "author")
    private String author;

    @Column(name = "iconUrl")
    private String iconUrl;

    @ElementCollection
    @Column(name = "steps")
    private List<String> steps;

    @Column(name = "publication")
    private RequestStatus publicationStatus;

    @ElementCollection
    @CollectionTable(name = "rating_mapping",
            joinColumns = {@JoinColumn(name = "rate_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "rate_name")
    @Column(name = "rates")
    private Map<String,Integer> rates;

    public Recipe(Long id) {
        this.id = id;
    }

    public Recipe(Long id, String title, String description, List<String> tags,
                  List<RecipeIngredient> ingredients, Date updated,
                  String author, String iconUrl, List<String> steps,
                  RequestStatus publicationStatus, Map<String, Integer> rates) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.ingredients = ingredients;
        this.updated = updated;
        this.author = author;
        this.iconUrl = iconUrl;
        this.steps = steps;
        this.publicationStatus = publicationStatus;
        this.rates = rates;
    }

    public Recipe() {
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<RecipeIngredient> getIngredients() {
        return ingredients;
    }

    public Date getUpdated() {
        return updated;
    }

    public String getAuthor() {
        return author;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public List<String> getSteps() {
        return steps;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setIngredients(List<RecipeIngredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public void setSteps(List<String> steps) {
        this.steps = steps;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RequestStatus getPublicationStatus() {
        return publicationStatus;
    }

    public void setPublicationStatus(RequestStatus publicationStatus) {
        this.publicationStatus = publicationStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Integer> getRates() {
        return rates;
    }

    public void setRates(Map<String, Integer> rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", tags=" + tags +
                ", ingredients=" + ingredients +
                ", updated=" + updated +
                ", author='" + author + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", steps=" + steps +
                ", publicationStatus=" + publicationStatus +
                '}';
    }

    public static RecipeBuilder builder() {
        return new RecipeBuilder();
    }

    public static final class RecipeBuilder {
        private Long id;
        private String title;
        private String description;
        private List<String> tags = new ArrayList<>();
        private List<RecipeIngredient> ingredients = new ArrayList<>();
        private Date updated;
        private String author;
        private String iconUrl;
        private List<String> steps;
        private RequestStatus publicationStatus;

        private RecipeBuilder() {
        }

        public static RecipeBuilder aRecipe() {
            return new RecipeBuilder();
        }

        public RecipeBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public RecipeBuilder withTitle(String title) {
            this.title = title;
            return this;
        }

        public RecipeBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public RecipeBuilder withTags(List<String> tags) {
            this.tags.addAll(tags);
            return this;
        }

        public RecipeBuilder withTags(String... tags) {
            this.tags.addAll(Arrays.asList(tags));
            return this;
        }

        public RecipeBuilder withIngredients(List<RecipeIngredient> ingredients) {
            this.ingredients = ingredients;
            return this;
        }

        public RecipeBuilder withUpdated(Date updated) {
            this.updated = updated;
            return this;
        }

        public RecipeBuilder withAuthor(String author) {
            this.author = author;
            return this;
        }

        public RecipeBuilder withIconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
            return this;
        }

        public RecipeBuilder withSteps(List<String> steps) {
            this.steps = steps;
            return this;
        }

        public RecipeBuilder withPublicationStatus(RequestStatus publicationStatus) {
            this.publicationStatus = publicationStatus;
            return this;
        }

        public Recipe build() {
            Recipe recipe = new Recipe(id);
            recipe.setTitle(title);
            recipe.setDescription(description);
            recipe.setTags(tags);
            recipe.setIngredients(ingredients);
            recipe.setUpdated(updated);
            recipe.setAuthor(author);
            recipe.setIconUrl(iconUrl);
            recipe.setSteps(steps);
            recipe.setPublicationStatus(publicationStatus);
            return recipe;
        }
    }
}

package ru.nsu.fit.alcoweb.alcobackend;

import ru.nsu.fit.alcoweb.alcobackend.model.api.DetailedRecipeDescription;
import ru.nsu.fit.alcoweb.alcobackend.model.api.Ingredient;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RecipeContent;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RecipeShortDescription;
import ru.nsu.fit.alcoweb.alcobackend.model.api.SearchRequest;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.Recipe;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RecipeIngredient;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RecipeSearchParameters;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RequestStatus;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.User;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DTOConverter {

    public static Recipe fromDto(DetailedRecipeDescription description) {
        return new Recipe(description.getId(), description.getTitle(), description.getDescription(),
                description.getTags(),
                description.getIngredients().stream()
                        .map(DTOConverter::fromDto)
                        .collect(Collectors.toList()),
                description.getUpdated(), description.getAuthor(), description.getIconUrl(),
                description.getSteps(), RequestStatus.NONE, Map.of());
    }

    public static Recipe fromDto(RecipeContent description, String author) {
        return new Recipe(null, description.getTitle(), description.getDescription(),
                description.getTags(),
                description.getIngredients().stream()
                        .map(DTOConverter::fromDto)
                        .collect(Collectors.toList()),
                null, author, description.getIconUrl(),
                description.getSteps(), RequestStatus.NONE, Map.of());
    }

    public static RecipeIngredient fromDto(Ingredient ingredient) {
        return new RecipeIngredient(ingredient.getLabel(), ingredient.getValue());
    }

    public static Ingredient toDto(RecipeIngredient ingredient) {
        var result = new Ingredient();
        result.setLabel(ingredient.getLabel());
        result.setValue(ingredient.getValue());
        return result;
    }

    public static DetailedRecipeDescription toDetailedRecipeDto(Recipe recipe, @Nullable User user) {
        DetailedRecipeDescription description = new DetailedRecipeDescription();
        description.setId(recipe.getId());
        description.setDescription(recipe.getDescription());
        description.setAuthor(recipe.getAuthor());
        description.setIngredients(Optional.ofNullable(recipe.getIngredients())
                .map(i -> i.stream().map(DTOConverter::toDto)
                        .collect(Collectors.toList()))
                .orElse(List.of()));
        description.setTags(recipe.getTags());
        description.setSteps(recipe.getSteps());
        description.setTitle(recipe.getTitle());
        description.setIconUrl(recipe.getIconUrl());
        description.setRating((float) recipe.getRates().values().stream()
                .mapToInt(Integer::intValue).average().orElse(0.0));
        description.setRatingNums((long) recipe.getRates().size());
        description.setUpdated(recipe.getUpdated());
        description.favourite(user != null && user.getFavourites().stream().map(Recipe::getId)
                .anyMatch(id -> id.equals(recipe.getId())))
                .rated(getRated(recipe, user));
        return description;
    }

    public static RecipeSearchParameters fromDto(SearchRequest request) {
        return new RecipeSearchParameters(
                request.getLimit(), request.getOffset(),
                request.getAuthor(), request.getQuery(),
                request.getTag(), request.getIngredient());
    }

    public static RecipeShortDescription toShortDescription(Recipe recipe, @Nullable User user) {
        return new RecipeShortDescription()
                .title(recipe.getTitle())
                .description(recipe.getDescription())
                .ingredients(recipe.getIngredients().stream().map(DTOConverter::toDto).collect(Collectors.toList()))
                .id(recipe.getId())
                .author(recipe.getAuthor())
                .iconUrl(recipe.getIconUrl())
                .rating((float) recipe.getRates().values().stream()
                        .mapToInt(Integer::intValue).average().orElse(0.0))
                .ratingNums((long) recipe.getRates().size())
                .tags(recipe.getTags())
                .description(recipe.getDescription())
                .updated(recipe.getUpdated())
                .favourite(user != null && user.getFavourites().stream().map(Recipe::getId)
                        .anyMatch(id -> id.equals(recipe.getId())))
                .rated(getRated(recipe, user));
    }

    private static Integer getRated(Recipe recipe, @Nullable User user) {
        if (user == null || !recipe.getRates().containsKey(user.getUsername())) return 0;
        return recipe.getRates().get(user.getUsername());
    }
}

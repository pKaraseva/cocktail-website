package ru.nsu.fit.alcoweb.alcobackend.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.RequestStatus;
import ru.nsu.fit.alcoweb.alcobackend.mvc.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    List<User> findAllByModerationRequest(RequestStatus status);
}
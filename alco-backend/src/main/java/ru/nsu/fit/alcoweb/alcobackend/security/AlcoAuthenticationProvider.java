package ru.nsu.fit.alcoweb.alcobackend.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.nsu.fit.alcoweb.alcobackend.user.UserPrincipal;
import ru.nsu.fit.alcoweb.alcobackend.user.UserService;

@Component
public class AlcoAuthenticationProvider implements AuthenticationProvider {

    Logger log = LoggerFactory.getLogger(AlcoAuthenticationProvider.class);

    private final UserService userService;

    @Autowired
    public AlcoAuthenticationProvider(UserService userService) {
        this.userService = userService;
    }

    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        String username = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();

        UserPrincipal userPrincipal = userService.loadUserByUsername(username);
        if (userPrincipal == null || !userPrincipal.getPassword().equals(password)) {
            throw new UsernameNotFoundException("Incorrect username/password");
        }
        return new UsernamePasswordAuthenticationToken(userPrincipal.getUsername(), userPrincipal.getPassword(), userPrincipal.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}

package ru.nsu.fit.alcoweb.alcobackend.mvc.controller;

import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.alcoweb.alcobackend.api.AccountApi;
import ru.nsu.fit.alcoweb.alcobackend.model.api.LoginData;
import ru.nsu.fit.alcoweb.alcobackend.model.api.ModelApiResponse;
import ru.nsu.fit.alcoweb.alcobackend.model.api.ModerationResponse;
import ru.nsu.fit.alcoweb.alcobackend.model.api.RegisterData;
import ru.nsu.fit.alcoweb.alcobackend.model.api.User;
import ru.nsu.fit.alcoweb.alcobackend.model.api.UserProfile;
import ru.nsu.fit.alcoweb.alcobackend.user.UserPrincipal;
import ru.nsu.fit.alcoweb.alcobackend.user.UserRole;
import ru.nsu.fit.alcoweb.alcobackend.user.UserService;

import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.USERNAME_TAG;
import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.ok;
import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.session;
import static ru.nsu.fit.alcoweb.alcobackend.mvc.controller.ControllerUtility.username;

@RestController
public class UserController implements AccountApi {

    Logger log = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @CrossOrigin
    @Override
    public ResponseEntity<Boolean> accountExist(String username) {
        return ResponseEntity.ok(userService.loadUser(username) != null);
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> userRegister(RegisterData userData) {
        if (userData.getUsername() == null) {
            throw new IllegalArgumentException("Username was not provided");
        }
        if (userData.getPassword() == null) {
            throw new IllegalArgumentException("Password was not provided");
        }
        if (userService.loadUser(userData.getUsername()) != null) {
            throw new IllegalArgumentException("User already exists");
        }
        userService.createUser(userData.getUsername(), userData.getPassword());
        return ResponseEntity.ok(ok());
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> userAuth(LoginData userData) {
        var user = userService.loadUserByUsername(Objects.requireNonNullElse(userData.getUsername(), ""));
        if (user != null && Objects.equals(user.getPassword(), userData.getPassword())) {
            session().setAttribute(ControllerUtility.USERNAME_TAG, userData.getUsername());
        } else {
            throw new AccessDeniedException("Invalid login or password");
        }
        return ResponseEntity.ok(ok());
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModerationResponse> checkModeratorRequestStatus() {
        var user = userService.loadUserByUsername(username());
        return AccountApi.super.checkModeratorRequestStatus();
    }

    @CrossOrigin
    @Override
    public ResponseEntity<User> getUserInfo() {
        var user = userService.loadUserByUsername(username());
        return ResponseEntity.ok(
                new User()
                        .username(user.getUsername())
                        .status(toUserStatus(getStatus(user))));
    }

    @CrossOrigin
    @Override
    public ResponseEntity<UserProfile> getUserInfoByLogin(String username) {
        var user = userService.loadUserByUsername(username());

        return ResponseEntity.ok(
                new UserProfile()
                        .username(user.getUsername())
                        .status(getStatus(user)));
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> postRequestToBeModerator() {
        userService.requestModeratorRole(username());
        return ResponseEntity.ok(ok());
    }

    @CrossOrigin
    @Override
    public ResponseEntity<ModelApiResponse> userLogout() {
        session().removeAttribute(USERNAME_TAG);
        return ResponseEntity.ok(ok());
    }

    private UserProfile.StatusEnum getStatus(UserPrincipal user) {
        UserProfile.StatusEnum status = UserProfile.StatusEnum.USER;
        var authorities = user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        if (authorities.contains(UserRole.MODERATOR.name())) {
            status = UserProfile.StatusEnum.MODERATOR;
        }
        if (authorities.contains(UserRole.ADMIN.name())) {
            status = UserProfile.StatusEnum.ADMIN;
        }
        return status;
    }

    private User.StatusEnum toUserStatus(UserProfile.StatusEnum status) {
        switch (status) {
            case USER:
                return User.StatusEnum.USER;
            case MODERATOR:
                return User.StatusEnum.MODERATOR;
            case ADMIN:
                return User.StatusEnum.ADMIN;
            default:
                throw new RuntimeException("Unknown user status");
        }
    }
}

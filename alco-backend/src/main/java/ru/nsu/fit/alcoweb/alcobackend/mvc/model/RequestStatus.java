package ru.nsu.fit.alcoweb.alcobackend.mvc.model;

public enum RequestStatus {
    NONE,
    ACCEPTED,
    DECLINED,
    PENDING
}

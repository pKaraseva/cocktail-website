package ru.nsu.fit.alcoweb.alcobackend.mvc.controller;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.nsu.fit.alcoweb.alcobackend.model.api.ModelApiResponse;

public class ControllerUtility {
    public static final String USERNAME_TAG = "username";

    public static ModelApiResponse ok() {
        return new ModelApiResponse().code(0);
    }


    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }

    public static String username() {
        return Optional.ofNullable(session().getAttribute(USERNAME_TAG)).map(Object::toString).orElse("anon");
    }
}
